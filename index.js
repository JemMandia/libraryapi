var mysql = require('mysql');
var express = require('express');
var cors = require('cors');
var app = express();

app.use(express.json());
app.use(cors());

var connection = mysql.createConnection ({
        host: 'localhost',
        user: 'root',
        password: '',
        database : 'library'
});
app.get('/usersList', function(request, response){
    
    connection.query("SELECT  * FROM  users",  function(err, rows, fields)
    {      
        if(err){
            response.status(500).send();
        }else{
            result = Object.values(JSON.parse(JSON.stringify(rows)))
            response.status(200).send(result);
        }
    });
})
app.get('/employeeList', function(request, response){
    
    connection.query("SELECT  * FROM  employee",  function(err, rows, fields)
    {      
        if(err){
            response.status(500).send();
        }else{
            result = Object.values(JSON.parse(JSON.stringify(rows)))
            response.status(200).send(result);
        }
    });
})
app.get('/logs', function(request, response){
    
    connection.query("SELECT a.tran_id,a.book_id,b.book_name,c.user_fullname,d.employee_name,DATE_FORMAT(a.date_borrowed, '%M %d %Y') as date_borrowed,IFNULL(DATE_FORMAT(a.date_returned, '%M %d %Y'),'Unreturned') as date_returned FROM logs a LEFT JOIN books b ON a.book_id = b.book_id LEFT JOIN users c ON a.user_id = c.user_id LEFT JOIN employee d ON a.employee_id = d.employee_id",  function(err, rows, fields)
    {      
        if(err){
            response.status(500).send();
        }else{
            result = Object.values(JSON.parse(JSON.stringify(rows)))
            response.status(200).send(result);
        }
    });
})
app.get('/books', function(request, response){
    
    connection.query("SELECT  * FROM  books",  function(err, rows, fields)
    {      
        if(err){
            response.status(500).send();
        }else{
            result = Object.values(JSON.parse(JSON.stringify(rows)))
            response.status(200).send(result);
        }
    });
})
app.post('/users/', function(request, response){
    user_uname = request.body.user_uname;
    user_password = request.body.user_password;
    connection.query(`SELECT * FROM  users WHERE user_uname = '${user_uname}' AND user_password = '${user_password}'`,  function(err, rows, fields)
    {      
        if(err){
            response.status(500).send();
        }else{
            result = Object.values(JSON.parse(JSON.stringify(rows)))
            response.status(200).send(result);
        }
    });
})
app.post('/books/', function(request, response){

    book_name = request.body.book_name;
    book_author = request.body.book_author;
    coverImg = request.body.coverImg;
    
    connection.query(
        "insert into books (book_name,book_author,isAvailable,coverImg) values ('"+book_name+"','"+book_author+"', 1,'"+coverImg+"')", 
            function(err, rows, fields)
    {      
        if(err){
            response.status(500).send();
        }else{
            response.status(200).send();
        }
    });
})
app.post('/logs/', function(request, response){

    book_id = request.body.book_id;
    user_id = request.body.user_id;
    employee_id = request.body.employee_id;
    date_borrowed = request.body.date_borrowed;
    
    connection.query(
        "insert into logs (book_id,user_id,employee_id,date_borrowed) values ('"+book_id+"','"+user_id+"','"+employee_id+"','"+date_borrowed+"')", 
            function(err, rows, fields)
    {      
        if(err){
            response.status(500).send();
        }else{
            response.status(200).send();
        }
    });
})
app.post('/users/:id', function(request, response){
    
    const { id } = request.params;
    connection.query(
        `SELECT * FROM users where user_id = '${id}'`, 
            function(err, rows, fields)
    {      
        if(err){
            response.status(500).send();
        }else{
            result = Object.values(JSON.parse(JSON.stringify(rows)))
            response.status(200).send(result);
        }
    });
})
app.delete('/deleteLogs/:id', function(request, response){
    
    const { id } = request.params;
    connection.query(
        `delete from logs where tran_id = '${id}'`, 
            function(err, rows, fields)
    {      
        if(err){
            response.status(500).send();
        }else{
            console.log('Deleted Succesfully!');
            response.status(200).send();
        }
    });
})
app.put('/updateLogs/:id', function(request, response){
    const { id } = request.params;
    date_returned = request.body.date_returned;
    console.log(date_returned)
    connection.query(
        `update logs set date_returned = '${date_returned}' WHERE tran_id = '${id}'`, 
            function(err, rows, fields)
    {      
        if(err){
            response.status(500).send();
        }else{
            console.log('Updated Succesfully!!!!!!!');
            response.status(200).send();
        }
    });
})

app.listen(4000);